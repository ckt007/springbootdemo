package com.ckt.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Column;

@Entity  // 标明这是一个 JPA 实体类

public class EuropeChampionsLeagueHistory {

    @Id  // 主键
    @Column(name = "id")
    private Integer id;

    @Column(name = "year")
    private String year;

    @Column(name = "winner")
    private String winner;

    @Column(name = "loser")
    private String loser;

    @Column(name = "final_score")
    private String finalScore;

    @Column(name = "winner_country")
    private String winnerCountry;

    @Column(name = "loser_country")
    private String loserCountry;

    // Getter 和 Setter 方法
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getWinner() {
        return winner;
    }

    public void setWinner(String winner) {
        this.winner = winner;
    }

    public String getLoser() {
        return loser;
    }

    public void setLoser(String loser) {
        this.loser = loser;
    }

    public String getFinalScore() {
        return finalScore;
    }

    public void setFinalScore(String finalScore) {
        this.finalScore = finalScore;
    }

    public String getWinnerCountry() {
        return winnerCountry;
    }

    public void setWinnerCountry(String winnerCountry) {
        this.winnerCountry = winnerCountry;
    }

    public String getLoserCountry() {
        return loserCountry;
    }

    public void setLoserCountry(String loserCountry) {
        this.loserCountry = loserCountry;
    }
}