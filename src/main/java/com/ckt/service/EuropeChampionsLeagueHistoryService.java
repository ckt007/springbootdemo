package com.ckt.service;

import com.ckt.model.EuropeChampionsLeagueHistory;
import com.ckt.repository.EuropeChampionsLeagueHistoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EuropeChampionsLeagueHistoryService {

    @Autowired
    private EuropeChampionsLeagueHistoryRepository repository;

    // 获取所有历史记录
    public List<EuropeChampionsLeagueHistory> getAllHistory() {
        return repository.findAll();
    }

    // 根据 ID 获取历史记录
    public EuropeChampionsLeagueHistory getHistoryById(Integer id) {
        return repository.findById(id).orElse(null);
    }

    // 创建历史记录
    public EuropeChampionsLeagueHistory createHistory(EuropeChampionsLeagueHistory history) {
        return repository.save(history);
    }

    // 删除历史记录
    public void deleteHistory(Integer id) {
        repository.deleteById(id);
    }

    // 根据赢家查询所有历史记录
    public List<EuropeChampionsLeagueHistory> getHistoryByWinner(String winner) {
        return repository.findByWinner(winner);  // 返回多个匹配的记录
    }
}
