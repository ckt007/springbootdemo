package com.ckt.repository;

import com.ckt.model.EuropeChampionsLeagueHistory;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.List;

public interface EuropeChampionsLeagueHistoryRepository extends JpaRepository<EuropeChampionsLeagueHistory, Integer> {

    // 根据赢家查询所有历史记录
    List<EuropeChampionsLeagueHistory> findByWinner(String winner);
}
