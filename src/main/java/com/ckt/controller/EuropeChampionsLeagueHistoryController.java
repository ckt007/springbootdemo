package com.ckt.controller;

import com.ckt.model.EuropeChampionsLeagueHistory ;
import com.ckt.service.EuropeChampionsLeagueHistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/history")
public class EuropeChampionsLeagueHistoryController {

    @Autowired
    private EuropeChampionsLeagueHistoryService service;

    // 获取所有历史记录
    @GetMapping
    public List<EuropeChampionsLeagueHistory> getAllHistory() {
        return service.getAllHistory();
    }

    // 根据 ID 获取历史记录
    @GetMapping("/{id}")
    public EuropeChampionsLeagueHistory getHistoryById(@PathVariable Integer id) {
        return service.getHistoryById(id);
    }

    // 创建历史记录
    @PostMapping
    public EuropeChampionsLeagueHistory createHistory(@RequestBody EuropeChampionsLeagueHistory history) {
        return service.createHistory(history);
    }

    // 根据 ID 删除历史记录
    @DeleteMapping("/{id}")
    public void deleteHistory(@PathVariable Integer id) {
        service.deleteHistory(id);
    }

    // 根据赢家查询所有历史记录
    @GetMapping("/winner/{winner}")
    public List<EuropeChampionsLeagueHistory> getHistoryByWinner(@PathVariable String winner) {
        return service.getHistoryByWinner(winner);
    }
}
