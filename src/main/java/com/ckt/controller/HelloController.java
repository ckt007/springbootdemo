package com.ckt.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

//@RestController
//public class HelloController {
//
//    @GetMapping("/hello")
//    public String hello(){
//        return "hello SpringBoot";
//    }
//}

@Controller
public class HelloController {

    @GetMapping("/hello")
    public String hello(){
        return "hello";
    }
    @RequestMapping("/index")
    public String index(){
        return "index";
    }
}